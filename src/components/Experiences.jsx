import React from "react";
import panel from "../assets/panel.svg"

export default function Experiences() {
    return (
        <div className="exp">
            <div className="row justify-content-md-center">
                <div className="col col-md-auto">
                    <img src={panel} className="exp-img" alt="Projects" />
                </div>
            </div>
            <div className="row justify-content-center">
                <div className="col col-10">
                    <div className="col-6">
                        <h1>Online Experiences</h1>
                        <span>Join unique interactive activities led by<br /> one-of-a-kind hosts—all without leaving home.</span>
                    </div>
                </div>
            </div>
        </div>
    )
}