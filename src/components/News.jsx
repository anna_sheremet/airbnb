import React from "react";
import ReactDOM from "react-dom/client";
import Card from "./Card.jsx";
import data from "./data"

export default function News() {

    const cards = data.map((post) => <Card
        key={post.id}
        {...post}
        // coverImg={post.coverImg}
        // stats={post.stats}
        // location={post.location}
        // title={post.title}
        // description={post.description}
        // price={post.price}
        // openSpots={post.openSpots}
    />)

    return (
        <div className="news">
            <div className="container text-center">
                <div className="row row-cols-1 row-cols-sm-2 row-cols-md-4 justify-content-md-around cards-container">
                    {cards}
                </div>
            </div>
        </div>
    )
}