import React from "react";
import logo from "../assets/logo.svg"

export default function Navbar() {
    return (
        <nav className="navbar bg-body-tertiary row nav">
            <div className="container-fluid col-10">
                <img src={logo} alt="airbnb" className="d-inline-block align-text-top nav-img" />
            </div>
        </nav>
    )
}