import React from "react";
import star from "../assets/star.svg"
import circle from "../assets/circle.svg"

export default function Card (props) {

    let badgeText
    if (props.openSpots === 0) {
        badgeText = "SOLD OUT"
    } else if (props.location === "Online") {
        badgeText = "ONLINE"
    }

    return (
        <div className="col">
            <div className="card">
                {badgeText && <div className="card-badge">{badgeText}</div>}
                <img src={`./images/${props.coverImg}`} className="card-img-top card-img" alt="..." />
                <div className="card-body">
                    <p>
                        <img src={star} alt="." className="star"  />
                        {props.stats.rating} ({props.stats.reviewCount})
                        <img src={circle} alt="." className="circle" />
                        {props.location}
                    </p>
                    <p className="card-text">{props.title}</p>
                    <p><b>From {props.price}$</b>/ person</p>
                </div>
            </div>
        </div>
    )
}