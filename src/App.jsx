import React from 'react'
import Navbar from "./components/Navbar.jsx";
import Experiences from "./components/Experiences.jsx";
import News from "./components/News.jsx";

function App() {

  return (
    <div className="App">
      <Navbar />
      <Experiences />
      <News />
    </div>
  )
}

export default App
